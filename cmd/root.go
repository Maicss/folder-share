/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"FolderShare/server"
	"os"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   os.Args[0] + " 文件夹路径",
	Short: "便捷的搭建用于文件共享的http服务器",
	Long:  `该程序可以将任意文件夹作为根目录创建http服务器以供其他用户访问。`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 1 {
			fileInfo, err := os.Stat(args[0])
			if err != nil {
				println("错误：", err.Error())
				return
			}
			if !fileInfo.IsDir() {
				println("参数必须是一个文件夹。")
				return
			}
			server.RunServer(args[0])
		} else {
			err := cmd.Help()
			if err != nil {
				return
			}
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().IntVarP(&server.InternalConfig.Port, "port", "p", 8080, "端口号设置")
	rootCmd.Flags().StringVarP(&server.InternalConfig.Host, "host", "H", "0.0.0.0", "指定服务主机")
	rootCmd.Flags().BoolVarP(&server.Config.AllowUpload, "allow-update", "u", false, "支持上传")
	rootCmd.Flags().BoolVarP(&server.Config.AllowGetFolder, "allow-get-folder", "f", false, "支持下载文件夹（实验性）\n"+
		"该选项将允许客户端请求文件夹以打包的形式下载，这\n"+
		"将占用服务器大量资源（运算资源和空间资源），您应\n"+
		"当只在小规模应用场景中打开此选项。")
}
