package server

import (
	"os"
	"path/filepath"
)

func IsHiddenFile(filename string) (bool, error) {
	// unix/linux file or directory that starts with . is hidden
	_, err := os.Stat(filename)

	if err != nil {
		return false, err
	}

	baseName := filepath.Base(filename)
	if baseName[0:1] == "." {
		return true, nil
	}
	return false, nil
}
