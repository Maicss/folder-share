package server

type internalConfig struct {
	Port int
	Host string
}

type config struct {
	AllowUpload    bool
	AllowGetFolder bool
}

// Config 公共配置信息，可以使用config接口获取
var Config config

// InternalConfig 内部配置信息，无法使用config接口获取
var InternalConfig internalConfig
