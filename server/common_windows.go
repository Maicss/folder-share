package server

import (
	"os"
	"syscall"
)

func IsHiddenFile(filename string) (bool, error) {

	_, err := os.Stat(filename)

	if err != nil {
		return false, err
	}

	pointer, err := syscall.UTF16PtrFromString(filename)
	if err != nil {
		return false, err
	}
	attributes, err := syscall.GetFileAttributes(pointer)
	if err != nil {
		return false, err
	}
	return attributes&syscall.FILE_ATTRIBUTE_HIDDEN != 0, nil
}
