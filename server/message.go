package server

// 消息常量列表
const (
	ErrMessOutDir = "正在尝试访问其他在共享范围外的内容，虽然我们已经拒绝他，但这通常意味着该主机存在非法倾向。"

	RequestAbnormalAccess = "非法访问"

	RequestNotFound = "没有找到"

	RequestNotADirectory = "不是一个文件夹"

	RequestIsExist = "已存在"

	RequestSuccess = "成功"
)
