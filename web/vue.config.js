module.exports = {
  transpileDependencies: true,
  publicPath: "./",
  devServer: {
    proxy: {
      "/list": {
        target: "http://192.168.191.206:8081/list",
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          "^/list": "",
        },
      },
      "/config": {
        target: "http://192.168.191.206:8081/config",
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          "^/config": "",
        },
      },
      "/file": {
        target: "http://192.168.191.206:8081/file",
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          "^/file": "",
        },
      },
      "/folder": {
        target: "http://192.168.191.206:8081/folder",
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          "^/folder": "",
        },
      },
    },
  },
};
