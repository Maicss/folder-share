import Vue from "vue";
import Vuex from "vuex";
import changeShow from "./changeShow";
import documents from "./documents";
import downloadFolderPath from "./downloadFolderPath";
import downloadFilePath from "./downloadFilePath";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    changeShow,
    documents,
    downloadFolderPath,
    downloadFilePath,
  },
});
