const downloadFolderPath = {
  state: {
    path: "",
  },
  mutations: {
    downloadFolderPath(state, path) {
      return (state.path = path);
    },
  },
  actions: {
    downloadFolderPath(context, path) {
      let src = "/folder?path=" + path;
      context.commit("downloadFolderPath", src);
    },
  },
};

export default downloadFolderPath;
