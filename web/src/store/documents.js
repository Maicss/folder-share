import axios from "axios";
import Stack from "@/comm/stack";
let path = "/list";
const historyPath1 = new Stack();
const historyPath2 = new Stack();
const getjson = (context, Path, mutations) => {
  let encodePath = encodeURIComponent(Path.peek());
  axios
    .get(path + "?path=" + encodePath)
    .then((res) => {
      let getDocument = res.data;
      let dir = [];
      let noDir = [];
      //将文件和文件夹分类
      getDocument.forEach((item) => {
        switch (item.IsDir) {
          case true:
            return dir.push(item);
          case false:
            return noDir.push(item);
          default:
            return;
        }
      });
      getDocument = [...dir, ...noDir];
      // 根据mime type寻找对应图标
      getDocument = getDocument.map((item) => {
        if (item.MimeType.indexOf("text") != -1) {
          let index = item.MimeType.indexOf(";");
          item.MimeType = item.MimeType.substring(0, index);
          item.MimeType = item.MimeType.replace("/", "-x-");
          let url = item.MimeType + ".png";
          item.MimeType = require("../assets/Yaru-icon/" + url);
          return item;
        } else if (item.MimeType == "") {
          item.MimeType = require("../assets/Yaru-icon/folder.png");
          return item;
        } else if (item.MimeType.indexOf("image") == 0) {
          item.MimeType = require("../assets/Yaru-icon/image.png");
          return item;
        } else if (item.MimeType.indexOf("font") != -1) {
          item.MimeType = require("../assets/Yaru-icon/font.png");
          return item;
        } else {
          item.MimeType = require("../assets/Yaru-icon/application-octet-stream.png");
          return item;
        }
      });
      context.commit(mutations, {
        getDocument,
        path: historyPath1,
        goPath: historyPath2,
        error: null,
      });
    })
    .catch((error) => {
      context.commit(mutations, {
        getDocument: [],
        path: null,
        error,
      });
    });
};
const documents = {
  state: {
    get: {
      getDocument: [],
      historyPath: historyPath1,
      goPath: historyPath2,
      error: null,
    },
  },
  mutations: {
    //渲染时请求数据
    fetch(state, json) {
      return (state.get = json);
    },
    // 打开文件夹
    openFolder(state, json) {
      return (state.get = json);
    },
    // 回到根目录
    backRoot(state, json) {
      return (state.get = json);
    },
    // 后退一级目录
    backOff(state, json) {
      return (state.get = json);
    },
    // 返回刚才观看的目录
    backGo(state, json) {
      return (state.get = json);
    },
  },
  actions: {
    // 渲染时请求数据
    fetch(context) {
      historyPath1.push("");
      context.commit("fetch", {
        getDocument: [],
        path: historyPath1,
        goPath: historyPath2,
        error: null,
      });
      getjson(context, historyPath1, "fetch");
    },
    // 打开文件夹
    openFolder(context, Path) {
      historyPath1.push(Path);
      console.log(historyPath1.peek());
      context.commit("openFolder", {
        getDocument: [],
        path: historyPath1,
        goPath: historyPath2,
        error: null,
      });
      getjson(context, historyPath1, "openFolder");
    },
    // 会到根目录
    backRoot(context) {
      historyPath1.push("/");
      context.commit("backRoot", {
        getDocument: [],
        path: historyPath1,
        goPath: historyPath2,
        error: null,
      });
      getjson(context, historyPath1, "backRoot");
    },
    // 返回上一级目录
    backOff(context, Path) {
      if (historyPath1.peek() === "") return;
      console.log("backoff", Path);
      historyPath2.push(Path);
      historyPath1.pop(Path);
      console.log(historyPath1.peek());
      console.log(historyPath2.peek());

      context.commit("backOff", {
        getDocument: [],
        path: historyPath1,
        goPath: historyPath2,
        error: null,
      });

      getjson(context, historyPath1, "backOff");
    },
    // 回到上一个目录
    backGo(context, Path) {
      // console.log(historyPath2.peek());
      if (historyPath2.isEmpty()) return;
      historyPath1.push(Path);
      context.commit("backGo", {
        getDocument: [],
        path: historyPath1,
        goPath: historyPath2,
        error: null,
      });
      getjson(context, historyPath2, "backGo");
      historyPath2.pop(Path);
    },
  },
};

export default documents;
