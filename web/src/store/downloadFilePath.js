const downloadFilePath = {
  state: {
    path: "",
  },
  mutations: {
    downloadFilePath(state, path) {
      return (state.path = path);
    },
  },
  actions: {
    downloadFilePath(context, path) {
      let src = "/file?path=" + path;
      context.commit("downloadFilePath", src);
    },
  },
};
export default downloadFilePath;
