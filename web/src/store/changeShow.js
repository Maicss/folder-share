const changeShow = {
  state: {
    showWay: "list",
  },
  mutations: {
    listWay(state, way) {
      state.showWay = way;
    },
    gridWay(state, way) {
      state.showWay = way;
    },
  },
};
export default changeShow;
