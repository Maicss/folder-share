# FolderShare

FolderShare 顾名思义是一个**轻量**的基于**http协议**的文件远程访问工具。

它具有以下特点：
- 功能专一，不做过多工作
- 使用方便，没有学习成本
- 性能优异，运行速度可观

## 使用方法

```
该程序可以将任意文件夹作为根目录创建http服务器以供其他用户访问。

Usage:
  ./foldershare 文件夹路径 [flags]

Flags:
  -f, --allow-get-folder   支持下载文件夹（实验性）
                           该选项将允许客户端请求文件夹以打包的形式下载，这
                           将占用服务器大量资源（运算资源和空间资源），您应
                           当只在小规模应用场景中打开此选项。
  -u, --allow-update       支持上传
  -h, --help               help for ./foldershare
  -H, --host string        指定服务主机 (default "0.0.0.0")
  -p, --port int           端口号设置 (default 8080)
```

例如：

以家目录为根目录开启共享服务（默认端口8080）

```bash
foldershare ~
```

指定端口：

```bash
foldershare ~ -p 8081
```

指定host主机

```bash
foldershare ~ -H 0.0.0.0
```

其中`0.0.0.0`表示任意主机可访问，否则表示指定主机可访问。默认为`0.0.0.0`

在此之后在客户端访问：

```
http://ipaddress:port/view
```

即可进入Web页面。

## 开发手册

- [接口手册](docs/interface_manual.md)
- [web开发手册](docs/web_manual.md)
- [如何构建](docs/how_build.md)
- [图标绘制手册](docs/draw_logo.md)
