# 开发手册

## 编译

```bash
go build foldershare.go
```

## 部署

可执行文件的目录下必须存在web目录，用于存放web页面文件。

## 接口

### 获取服务器策略

- 请求方式：GET
- 接口名称：folder
- 请求格式：`http://ipaddress:port/config?path=<路径>`

该接口用于获得服务器对于特殊接口的开放状态。

### 获取文件MD5

- 请求方式：GET
- 接口名称：folder
- 请求格式：`http://ipaddress:port/md5?path=<路径>`

该接口用于获得文件的md5信息，该接口将直接返回md5字符串。

### 获取文件夹内容

- 请求方式：GET
- 接口名称：folder
- 请求格式：`http://ipaddress:port/folder?path=<路径>`

其中路径是以服务端指定的目录作为根目录的，相对路径绝对路径均可。

返回内容示例：
```json
[
  {
    "ID": 0,
    "Name": ".browserslistrc",
    "Size": 30,
    "IsDir": false,
    "IsRegular": true,
    "Path": ".browserslistrc",
    "MimeType": "application/octet-stream",
    "IsHidden": false
  },
  {
    "ID": 1,
    "Name": ".eslintrc.js",
    "Size": 359,
    "IsDir": false,
    "IsRegular": true,
    "Path": ".eslintrc.js",
    "MimeType": "text/javascript; charset=utf-8",
    "IsHidden": false
  }
]
```

解释：
- ID: 文件夹内容的编号。
- Name：文件或目录的名称。
- Size：文件的大小，以字节为单位(文件夹并非表示大小)。
- IsDir：是否是文件夹，值可能为true或者false。
- IsRegular：是否是常规文件。
- Path：访问该文件的相对路径(下边会提到)。
- MimeType：描述文件类型，若为文件夹则此项为空。
- IsHidden: 描述文件或文件夹是否为隐藏文件。

> 特别说明：
> 
> IsRegular 应当与 Size 搭配使用，当且仅当IsRegular为true时，Size的值才具有价值，Size值的意义将不确定。
> 
> MimeType的判定方式是先由后缀名判定，若后缀名判定失败，则通过文件头部内容判定。
> 若均得不到具体结果，则返回`application/octet-stream`表示未知类型。

### 获取文件

- 请求方式：GET
- 接口名称：file
- 请求格式：`http://ipaddress:port/file?path=<路径>`

这里的Path就是上边获取文件夹内容接口中的Path项。

### 获取文件夹

- 请求方式：GET
- 接口名称：file
- 请求格式：`http://ipaddress:port/folder?path=<路径>`

支持一次下载一整个文件夹，需要使用以下接口：

### 上传文件

要令服务器支持文件上传功能，则需要在服务器运行时使用`-u`选项，否则upload将返回404。

服务器支持单次上传多个文件。

上传文件前端代码示例：

```html
<form action="/upload?path=." method="post" enctype="multipart/form-data">
    上传文件:<input type="file" name="upload" multiple>
    <input type="submit" value="提交">
</form>
```

其中path参数用于指定上传的文件夹。

当请求发送后，可能得到如下反馈：

文件上传成功：
```json
{
  "message": "传输成功"
}
```

文件已存在导致传输失败：
```json
{
  "Error": "该文件已存在"
}
```

其他失败原因：
```json
{
  "Error": "失败原因信息"
}
```

### 前端页面

使用浏览器访问以下地址，可以使用我们提供的前端浏览页面：

```
http://ipaddress:port/view
```

### 错误请求

当请求在服务器中发现错误时，后端将返回一个错误信息，前端需要按照需要处理和展示。

返回代码：400

返回格式：

```json
{
  "Error": "错误信息"
}
```

