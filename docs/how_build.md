# 构建手册

## 在Linux上构建

### 后端构建
首先建立构建目录，用于存储构建结果：

```bash
mkdir build
```

然后补齐编译后端程序所需依赖：

```bash
go mod tidy
```

构建程序：

```bash
go build foldershare.go
```

将构建后的可执行程序移动至构建目录中：

```bash
mkdir build/opt/foldershare/web -p
mv foldershare build/opt/foldershare/
```

### 前端构建


